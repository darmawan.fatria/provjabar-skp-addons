from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import pooler

class project_perilaku_massive(osv.Model):
    _name = "project.perilaku.massive"
    _description = "Proses Select All perilaku"
    def verify_all_task(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        task_pool = pool_obj.get('project.perilaku')
        task_ids = task_pool.read(cr, uid, context['active_ids'], ['state','employee_id','komitmen_verifikasi_notsame','realisasi_jumlah_hari_kerja','realisasi_jumlah_jam_kerja',''], context=context)
        
        update_task_ids=[]
        for record in task_ids:
            if not record['employee_id'] :
                continue;
            if record['state'] not in ('evaluated'):
                continue;
            if not record['realisasi_jumlah_hari_kerja'] or not record['realisasi_jumlah_jam_kerja']  :
                continue;
            if record['realisasi_jumlah_hari_kerja'] == 0 or record['realisasi_jumlah_jam_kerja'] == 0 :
                continue;
            if record['komitmen_verifikasi_notsame']:
                continue;
            if task_pool.get_auth_id(cr, uid, record['id'], 'user_id_bkd', context):
                update_task_ids.append(record['id'])
        
        if update_task_ids:
           task_pool.action_done(cr, uid, update_task_ids, context)
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_perilaku_massive()

class project_perilaku_temp_calc_massive(osv.Model):
    _name = "project.perilaku.temp.calc.massive"
    _description = "Proses Select Perilaku Hitung All Nilai Sementara"
    def poin_calculation_temporary_all_task(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        task_pool = pool_obj.get('project.perilaku')
        task_ids = task_pool.read(cr, uid, context['active_ids'], ['state','employee_id',], context=context)
        
        update_task_ids=[]
        for record in task_ids:
            if not record['employee_id'] :
                continue;
            if record['state'] not in ('evaluated'):
                continue;
            
            update_task_ids.append(record['id'])
        
        if update_task_ids:
           task_pool.do_task_poin_calculation_temporary(cr, uid, update_task_ids, context)
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_perilaku_temp_calc_massive()

class project_perilaku_evaluated_massive(osv.Model):
    _name = "project.perilaku.evaluated.massive"
    _description = "Proses Select All perilaku | Atasan-BKD"
    def evaluated_all_task(self, cr, uid, ids, context=None):
        
        if context is None:
            context = {}
        pool_obj = pooler.get_pool(cr.dbname)
        task_pool = pool_obj.get('project.perilaku')
        task_ids = task_pool.read(cr, uid, context['active_ids'], ['state','employee_id'], context=context)
        
        update_task_ids=[]
        for record in task_ids:
            if not record['employee_id'] :
                continue;
            if record['state'] not in ('propose'):
                continue;
            update_task_ids.append(record['id'])
        
        if update_task_ids:
           task_pool.set_evaluated(cr, 1, update_task_ids, context)
                
                
        return {'type': 'ir.actions.act_window_close'}
   

project_perilaku_evaluated_massive()

