{
    "name": "Rekapitulasi Tahunan Kegiatan Per OPD",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Penilaian Prestasi Kerja / Rekapitulasi",
    "description": "Laporan Rekapitulasi Tahunan Kegiatan Per OPD ",
    "website" : "www.mediasee.net",
    "license" : "GPL-3",
    "depends": ['df_skp_employee'],
    'data': ["skp_yearly_recap_report.xml",],
    'installable': True,
    'active': False,
}
