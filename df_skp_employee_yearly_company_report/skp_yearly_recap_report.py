from openerp.osv import fields, osv
import time

class skp_yearly_recap_report(osv.osv_memory):
    
    _name = "skp.yearly.recap.report"
    _columns = {
        'company_id'        : fields.many2one('res.company', 'OPD'),
        'biro_id'           : fields.many2one('partner.employee.biro', 'Biro'),
        'period_year'       : fields.char('Periode Tahun', size=4, required=True),
        'is_kepala_opd'     : fields.boolean('Hanya Kepala OPD'),
        'format_print'      : fields.boolean('Format Standar',help="Format Standar Hanya menampilkan Nilai, Nama Dan NIP Tanpa Ketterangan Jabatan dll")
        
    }
    _defaults = {
        'period_year':lambda *args: time.strftime('%Y'),
        'format_print':False,
        'is_kepala_opd':False,
        
    }
    
    def get_skp_yearly_recap_report(self, cr, uid, ids, context={}):
        print 'printttttt yearly...'
        value = self.read(cr, uid, ids)[0]
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'skp.yearly.recap.report',
            'form': value
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'skp.yearly.recap.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
skp_yearly_recap_report()
